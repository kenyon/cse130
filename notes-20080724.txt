20080724 CSE 130 lecture notes

prolog

first([First|_],First).
rest([_|Rest],Rest).

member(Element,[Element|_]).
member(Element,[_|Rest]) :- member(Element,Rest).

assoc(Key,Alist,Val) :- member([Key,Val],Alist).

---------

a query: member(X,[23,45,67,12,222,19,9,6]), Y is X*X, Y<100.

