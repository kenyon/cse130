20080708 CSE 130 lecture notes

deferred substitution - optional for pa3

closures in ch. 6

scope - dynamic vs. static

procedure F (n : int ):
begin
    n := 1
end

var v : int;
v := 0;
F(v); // with pass-by-value (copy-in), v would still be 0 after function call

// with pass-by-reference, v would have value 1 after F call. n then is just an
// alias of v.

pass by value-result (copy-in, copy-out)

macro expansion (text substitution)

call by name
http://en.wikipedia.org/wiki/Call_by_name#Call_by_name


proc F (x, y)
var i : int;
begin
    v := v+1;
    v := i+3;
    x := x+2;
    y := y+1;
    A[0] := A[0]+1;
end

var v, i : int;
v := 0, A[v] := 1, i := 0;
F(i, A[i]);
