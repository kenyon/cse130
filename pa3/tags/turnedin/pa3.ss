#lang plai
;; You have to install the PLAI language before you can run this.
;; In DrScheme's File menu, select "Install .plt File..." and enter this URL:
;; http://www.cs.brown.edu/~sk/Publications/Books/ProgLangs/plai-4.plt

;; pa3.ss by Kenyon Ralph <kralph@ucsd.edu>

(define-type Binding
  [binding (name symbol?) (named-expr CFCFWAE?)])

(define-type CFCFWAE
  [num (n number?)]
  [binop (op procedure?) (lhs CFCFWAE?) (rhs CFCFWAE?)]
  [with (lob (listof Binding?)) (body CFCFWAE?)]
  [id (name symbol?)]
  [if0 (test-e CFCFWAE?) (then-e CFCFWAE?) (else-e CFCFWAE?)]
  [fun (args (listof symbol?)) (body CFCFWAE?)]
  [app (fun CFCFWAE?) (args (listof CFCFWAE?))])

;; the keywords used in this language
(define cfcfwae-keywords
  '(fun with if0))

;; the operators used in this language
(define cfcfwae-operators
  (list
   (list '+ +)
   (list '- -)
   (list '* *)
   (list '/ /)))

;; cfcfwae-keyword? : symbol -> boolean
;; true only if the symbol is a syntactic keyword of this language
(define (cfcfwae-keyword? sym)
  (if (member sym cfcfwae-keywords)
    #t
    #f))

;; cfcfwae-op? : symbol -> boolean
;; true only if the symbol is an operator name
(define (cfcfwae-op? sym)
  (if (assoc sym cfcfwae-operators)
    #t
    #f))

;; get-binary-operator : symbol -> procedure
;; example: (get-binary-operator '*) -> #<procedure:*>
(define (get-binary-operator opsymbol)
  (second (assoc opsymbol cfcfwae-operators)))

;; cfcfwae-reserved? : symbol -> boolean
;; true only if the symbol is a keyword or operator name
(define (cfcfwae-reserved? sym)
  (or (cfcfwae-op? sym) (cfcfwae-keyword? sym)))

;; make-binop : opsymbol, lhs, rhs -> parsed binop
;; example: (make-binop '+ 4 8) -> (binop #<procedure:+> (num 4) (num 8))
;; used in parse
(define (make-binop opsymbol lhs rhs)
  (binop (get-binary-operator opsymbol) (parse lhs) (parse rhs)))

;; parse-binding-list : list -> abstract syntax
;; used in parse
;; example: (parse-binding-list '{{x 2} {y 3}})
;; returns ((binding x (num 2)) (binding y (num 3)))
(define (parse-binding-list binding-list)
  (if (list? binding-list)
      (map parse-binding binding-list)
      (error "expected a list of bindings")))

;; parse-binding : list -> abstract syntax
;; used in parse-binding-list
;; example: (parse-binding '{x 2})
;; returns (binding x (num 2))
(define (parse-binding binding-pair)
  (cond
    [(not (pair? binding-pair))
     (error "expected list in binding of with expression")]
    [(empty? (cdr binding-pair))
     (error "expected (id val) in with expression")]
    [(not (symbol? (car binding-pair)))
     (error "identifiers must be symbols")]
    [(cfcfwae-reserved? (car binding-pair))
     (error "cannot bind a value to a reserved symbol")]
    [else
     (binding (first binding-pair) (parse (last binding-pair)))]))

;; good-sublists-form? : binding list -> true if the list is well-formed, false otherwise
;; checks for non-list elements in the binding list
;; used in parse
;; example: (good-sublists-form? '{{x 5} 24}) -> error
;; example: (good-sublists-form? '{{x 5} y}) -> error
;; example: (good-sublists-form? '{{x 5} {y 12}}) -> #t
(define (good-sublists-form? binding-list)
  (cond
    [(empty? binding-list)
     true]
    [(list? (first binding-list))
     (good-sublists-form? (rest binding-list))]
    [else
     false]))

;; valid-binary-operation? : list -> true if the argument is a valid binary operation,
;; false otherwise
;; used in parse
;; example: (valid-binary-operation? '{+ 1 45}) -> true
;; exmaple: (valid-binary-operation? '{nonsense 1 45}) -> error
(define (valid-binary-operation? sexp)
  (if (and (list? sexp) (equal? (length sexp) 3) (not (cfcfwae-reserved? (first sexp))))
      false
      (if (and
           (list? sexp)
           (not (empty? sexp))
           (cfcfwae-op? (first sexp))
           (< (length (rest sexp)) 2))
          (error "wrong number of arguments to binary operator")
          true)))

;; parse : s-exp -> CFCFWAE
;; consumes an s-expression and generates the corresponding CFCFWAE
;; indicates parsing errors using (error "appropriate message")
(define (parse sexp)
  (if (cfcfwae-keyword? sexp)
      (error "unexpected keyword") ;shouldn't see a keyword until we see a list
      (cond
        [(boolean? sexp) (error "invalid datatype in input")]
        [(symbol? sexp) (id sexp)]
        [(number? sexp) (num sexp)]
        [(list? sexp)
         (cond
           [(empty? sexp) (error "unexpected empty list")]
           [(and (cfcfwae-op? (first sexp)) (valid-binary-operation? sexp))
            (make-binop (first sexp) (second sexp) (third sexp))]
           [else
            (case (first sexp)
              [(with)
               (cond
                 [(< (length sexp) 3)
                  (error "malformed with expression")]
                 [(not (list? (second sexp)))
                  (error "expected a list of bindings")]
                 [(not (good-sublists-form? (second sexp)))
                  (error "expected list in binding of with expression")]
                 [(not (equal? (length (remove-duplicates (map first (second sexp))))
                               (length (map first (second sexp)))))
                  (error "duplicate bindings for identifier")]
                 [else
                  (with (parse-binding-list (second sexp))
                        (parse (third sexp)))])]
              [(if0)
               (cond
                 [(< (length sexp) 4)
                  (error "malformed if0 expression")]
                 [else
                  (if0
                   (parse (second sexp))
                   (parse (third sexp))
                   (parse (fourth sexp)))])]
              [(fun)
               (cond
                 [(< (length sexp) 3)
                  (error "malformed fun expression")]
                 [(not (list? (second sexp)))
                  (error "expected a list of formals")]
                 [(not (andmap symbol? (second sexp)))
                  (error "identifiers must be symbols")]
                 [(ormap cfcfwae-reserved? (second sexp))
                  (error "cannot bind a value to a reserved symbol")]
                 [(not (equal? (length (remove-duplicates (second sexp)))
                               (length (second sexp))))
                  (error "duplicate formal parameter name")]
                 [else
                  (fun (second sexp) (parse (third sexp)))])]
              [else
               (app (parse (first sexp)) (map parse (rest sexp)))])])])))

;; do-with-substitution : {<binding> ...} <CFCFWAE> -> <CFCFWAE>
;; returns substed-body for use in a with element
;; where substed-body is the original bound-body, but with substitution done on it.
;; input and output are in abstract syntax.
(define (do-with-substitution binding-list bound-body)
  (cond
    [(empty? binding-list) bound-body]
    [else
     (do-with-substitution (rest binding-list) (subst bound-body (binding-name (first binding-list)) (binding-named-expr (first binding-list))))]))

;; subst : CFCFWAE symbol CFCFWAE -> CFCFWAE
;; substitutes second argument with third argument in ﬁrst argument,
;; as per the rules of substitution; the resulting expression contains
;; no free instances of the second argument
;; direct invokation example: (subst (binop + (id 'x) (num 5)) 'x (num 2))
(define (subst expr sub-id val)
  (type-case CFCFWAE expr
             [id (v) (if (equal? v sub-id)
                         val
                         expr)]
             [num (n) expr]
             [binop (op l r) (binop op (subst l sub-id val) (subst r sub-id val))]
             [with (binding-list bound-body)
                   (let ([substed-body (do-with-substitution binding-list bound-body)])
                     (with binding-list substed-body))]
             [else
              (error "not implemented")]))

(define (do-funarg-subst funargs funbody arglist)
  (cond
    [(empty? funargs) funbody]
    [else
     (do-funarg-subst (rest funargs) (subst funbody (first funargs) (first arglist)) (rest arglist))]))

;; interp : CFCFWAE -> CFCFWAE
;; evaluates CFCFWAE expressions by reducing them to their corresponding values
;; return values are either num or fun
;; Indicates evaluation errors using (error "appropriate message")
(define (interp expr)
  (type-case CFCFWAE expr
     [num (n) n]
     [binop (op l r) (op (interp l) (interp r))]
     [with (binding-list bound-body)
           (cond
             [(not (empty? binding-list))
              (interp (with empty (do-with-substitution binding-list bound-body)))]
             [else
              (interp bound-body)])]
     [id (v) (error 'interp "free identifier")]
     [if0 (test-e then-e else-e)
          (let ([val (interp test-e)])
            (if (number? val)
                (if (zero? val)
                    (interp then-e)
                    (interp else-e))
                (error "expected a number")))]
     [fun (arglist body)
          expr]
     [app (funct arglist)
          (cond
            [(not (fun? funct))
             (error "free identifier")]
            [else
             (let ([funargs (fun-args funct)]
                   [funbody (fun-body funct)])
               (begin
                 ;(display funargs) (newline)
                 ;(display funbody) (newline)
                 ;(display arglist) (newline)
                 (cond
                   [(not (equal? (length arglist) (length funargs)))
                    (error "wrong number of args")]
                   [else
                    (interp (do-funarg-subst (fun-args funct) (fun-body funct) arglist))])))])]))
