(define anon-fact-5
  "{{fun {f x} {f f x 1}}
    {fun {f x p}
      {if0 x p
           {f f {- x 1} {* x p}}}}
    5}")

(define curried-anon-fact-6
  "{{{fun {f}
       {fun {x}
         {{{f f} x} 1}}}
     {fun {f}
       {fun {x}
         {fun {p}
           {if0 x p
                {{{f f} {- x 1}} {* x p}}}}}}}
     6}")

   (test-exn "interp"
             (expect-error "free identifier")
             (try-to-eval "{foo 1 2}"))
   (test-exn "parse"
             (expect-error "malformed if0 expression")
             (try-to-parse "{if0}"))
   (test-exn "interp"
             (expect-error "expected a number")
             (try-to-eval "{if0 {fun {} 0} 1 2}"))
   (test-exn "parse"
             (expect-error "malformed fun expression")
             (try-to-parse "{fun}"))
   (test-exn "parse"
             (expect-error "expected a list of formals")
             (try-to-parse "{fun x x}"))
   (test-exn "parse"
             (expect-error "identifiers must be symbols")
             (try-to-parse "{fun {1} 1}"))
   (test-exn "parse"
             (expect-error "cannot bind a value to a reserved symbol")
             (try-to-parse "{fun {with} 1}"))
   (test-exn "parse"
             (expect-error "duplicate formal parameter name")
             (try-to-parse "{fun {x x} 1}"))
   (test-exn "interp"
             (expect-error "free identifier")
             (try-to-eval "{f}"))
   (test-exn "interp"
             (expect-error "function expects 0 argument(s), given 1")
             (try-to-eval "{{fun {} 1} 1}"))
   (test-exn "interp"
             (expect-error "expected function, given: 1")
             (try-to-eval "{1 2}"))

   (test-equal? "interp" (read-and-eval-string "{{fun {x} x} 1}") 1)
   (test-equal? "interp"
                (read-and-eval-string "{with {{x 1}} {{fun {y} x} 2}}") 1)
   (test-equal? "interp"
                (read-and-eval-string "{with {{x 1}} {{fun {y} y} x}}") 1)
   (test-equal? "interp" (read-and-eval-string "{{fun {x y} x} 1 2}") 1)
   (test-equal? "interp" (read-and-eval-string "{{fun {x y} {+ x y}} 1 2}") 3)
   (test-equal? "parse"
                (read-and-parse-string "{if0 1 2 3}")
                (if0 (num 1) (num 2) (num 3)))
   (test-equal? "interp" (read-and-eval-string "{if0 0 1 2}") 1)
   (test-equal? "interp" (read-and-eval-string "{if0 -1 1 2}") 2)
   (test-equal? "interp" (read-and-eval-string "{if0 0 1 {/ 1 0}}") 1)
   (test-equal? "interp" (read-and-eval-string "{if0 1 {/ 1 0} 2}") 2)
   (test-exn "interp"
             (expect-error "free identifier")
             (try-to-eval "{with {} {bar 1 2}}"))
   (test-equal? "interp" (read-and-eval-string curried-fact-3) 6)
   (test-equal? "interp" (read-and-eval-string factorial-4) 24)
   (test-equal? "interp" (read-and-eval-string anon-fact-5) 120)
   (test-equal? "interp" (read-and-eval-string curried-anon-fact-6) 720)
