#lang scheme
(require "pa3.ss" (planet "test.ss" ("schematics" "schemeunit.plt" 2)))
(require (planet "text-ui.ss" ("schematics" "schemeunit.plt" 2)))
;; When first you run this, it will fetch and install schemeunit automatically.

;; pa3-tests.ss by Kenyon Ralph <kralph@ucsd.edu>

;; For use in tests. True if the correct error message is raised; false otherwise.
(define read-and-parse-string (compose parse read open-input-string))
(define read-and-eval-string (compose interp read-and-parse-string))
(define (try-to-parse input-string)
  (lambda () (read-and-parse-string input-string)))
(define (try-to-eval input-string)
  (lambda () (read-and-eval-string input-string)))

;; For use in tests. True if the correct error message is raised; false otherwise.
(define (expect-error msg)
  (lambda (exn) (string=? (exn-message exn) msg)))

(define my-value-is-7
  "{with {{x 2} {y 3}}
         {with {{z {+ x y}}}
               {+ x z}}}")

(define factorial-4
  "{with {{fact {fun {f n} {if0 n 1 {* n {f f {- n 1}}}}}}}
     {fact fact 4}}")

(define pa3-tests
  (test-suite
   "pa3"
   ;; Tests that should generate errors.
   (test-exn "parse1"
             (expect-error "unexpected keyword")
             (try-to-parse "with"))
   (test-exn "parse2"
             (expect-error "unexpected empty list")
             (try-to-parse "{}"))
   (test-exn "parse3"
             (expect-error "malformed with expression")
             (try-to-parse "{with}"))
   (test-exn "interp1" ;;;;;; was "invalid binary operator: foo" in pa2
             (expect-error "free identifier")
             (try-to-eval "{foo 1 2}"))
   (test-exn "parse4"
             (expect-error "wrong number of arguments to binary operator")
             (try-to-parse "{+ 1}"))
   (test-exn "parse5"
             (expect-error "invalid datatype in input")
             (try-to-parse "#t"))
   (test-exn "parse6"
             (expect-error "expected a list of bindings")
             (try-to-parse "{with x x}"))
   (test-exn "parse7"
             (expect-error "expected list in binding of with expression")
             (try-to-parse "{with {x} x}"))
   (test-exn "parse8"
             (expect-error "expected (id val) in with expression")
             (try-to-parse "{with {{x}} x}"))
   (test-exn "parse9"
             (expect-error "identifiers must be symbols")
             (try-to-parse "{with {{1 2}} x}"))
   (test-exn "parse10"
             (expect-error "cannot bind a value to a reserved symbol")
             (try-to-parse "{with {{with 2}} x}"))
   (test-exn "parse11"
             (expect-error "duplicate bindings for identifier")
             (try-to-parse "{with {{x 1} {x 2}} x}"))
   (test-exn "interp2"
             (expect-error "free identifier")
             (try-to-eval "x"))
   (test-exn "parse12"
             (expect-error "malformed if0 expression")
             (try-to-parse "{if0}"))
   (test-exn "interp3"
             (expect-error "expected a number")
             (try-to-eval "{if0 {fun {} 0} 1 2}"))
   (test-exn "parse13"
             (expect-error "malformed fun expression")
             (try-to-parse "{fun}"))
   (test-exn "parse14"
             (expect-error "expected a list of formals")
             (try-to-parse "{fun x x}"))
   (test-exn "parse15"
             (expect-error "identifiers must be symbols")
             (try-to-parse "{fun {1} 1}"))
   (test-exn "parse16"
             (expect-error "cannot bind a value to a reserved symbol")
             (try-to-parse "{fun {with} 1}"))
   (test-exn "parse17"
             (expect-error "duplicate formal parameter name")
             (try-to-parse "{fun {x x} 1}"))
   (test-exn "interp4"
             (expect-error "free identifier")
             (try-to-eval "{f}"))
   (test-exn "interp5"
             (expect-error "function expects 0 argument(s), given 1")
             (try-to-eval "{{fun {} 1} 1}"))
   (test-exn "interp6"
             (expect-error "expected function, given: 1")
             (try-to-eval "{1 2}"))
   (test-exn "parse18"
             (expect-error "expected list in binding of with expression")
             (try-to-parse "{with {{x 4} 9} x}"))

   ;; Tests that should succeed.
   (test-equal? "cfcfwae-keyword? 1" (cfcfwae-keyword? 'with) #t)
   (test-equal? "cfcfwae-keyword? 2" (cfcfwae-keyword? 'notwith) #f)
   (test-equal? "cfcfwae-keyword? 3" (cfcfwae-keyword? 'fun) #t)

   (test-equal? "cfcfwae-op? 1" (cfcfwae-op? '+) #t)
   (test-equal? "cfcfwae-op? 2" (cfcfwae-op? 'junk) #f)
   (test-equal? "cfcfwae-op? 3" (cfcfwae-op? '*) #t)

   (test-equal? "cfcfwae-reserved? 1" (cfcfwae-reserved? '-) #t)
   (test-equal? "cfcfwae-reserved? 2" (cfcfwae-reserved? 'junk) #f)
   (test-equal? "cfcfwae-reserved? 3" (cfcfwae-reserved? 'with) #t)
   (test-equal? "cfcfwae-reserved? 4" (cfcfwae-reserved? 'if0) #t)

   (test-equal? "interp7" (read-and-eval-string my-value-is-7) 7)
   (test-equal? "interp fact" (read-and-eval-string factorial-4) 24)

   (test-equal? "interp9" (read-and-eval-string "{if0 {+ 9 -9} 1 2}") 1)
   (test-equal? "interp10" (read-and-eval-string "{if0 {- 9 4} 1 2}") 2)
   (test-exn "interp11"
             (expect-error "free identifier")
             (try-to-eval "{if0 x 1 2}"))

   (test-equal? "interp12" (read-and-eval-string "{{fun {x} x} 2}") 2)
   (test-equal? "interp13" (read-and-eval-string "{{fun {x} {+ x 8}} 12}") 20)
   (test-equal? "interp14" (read-and-eval-string "{{fun {x y} {+ x y}} 10 4}") 14)
   ))

(test/text-ui pa3-tests)
