#lang plai

;; pa5.ss by Kenyon Ralph <kralph@ucsd.edu>

(define-type TFWAE
  [num (n number?)]
  [binop (op procedure?) (lhs TFWAE?) (rhs TFWAE?)]
  [with (name symbol?) (named-expr TFWAE?) (body TFWAE?)]
  [id (name symbol?)]
  [if0 (test-e TFWAE?) (then-e TFWAE?) (else-e TFWAE?)]
  [fun (arg-id symbol?) (arg-type Type?) (result-type Type?) (body TFWAE?)]
  [app (fun-expr TFWAE?) (arg-expr TFWAE?)])

(define-type Type
  [t-num]
  [t-fun (arg Type?) (result Type?)])

;; the basic types used in this language
(define tfwae-basic-types
  '(number))

;; tfwae-basic-type? : symbol -> boolean
;; true only if the symbol is a basic type of this language
(define (tfwae-basic-type? sym)
  (if (member sym tfwae-basic-types)
      #t
      #f))

;; type=? : Type Type -> boolean
(define (type=? t1 t2)
  (equal? t1 t2))

;; the keywords used in this language
(define tfwae-keywords
  '(fun with if0))

;; tfwae-keyword? : symbol -> boolean
;; true only if the symbol is a syntactic keyword of this language
(define (tfwae-keyword? sym)
  (if (member sym tfwae-keywords)
    #t
    #f))

;; the operators used in this language
(define tfwae-operators
  (list
   (list '+ +)
   (list '- -)
   (list '* *)
   (list '/ /)))

;; tfwae-op? : symbol -> boolean
;; true only if the symbol is an operator name
(define (tfwae-op? sym)
  (if (assoc sym tfwae-operators)
    #t
    #f))

;; tfwae-reserved? : symbol -> boolean
;; true only if the symbol is a keyword or operator name
(define (tfwae-reserved? sym)
  (or (tfwae-op? sym) (tfwae-keyword? sym)))

;; get-binary-operator : symbol -> procedure
;; example: (get-binary-operator '*) -> #<procedure:*>
(define (get-binary-operator opsymbol)
  (second (assoc opsymbol tfwae-operators)))

;; make-binop : opsymbol, lhs, rhs -> parsed binop
;; example: (make-binop '+ 4 8) -> (binop #<procedure:+> (num 4) (num 8))
;; used in parse
(define (make-binop opsymbol lhs rhs)
  (binop (get-binary-operator opsymbol) (parse lhs) (parse rhs)))

;; parse-binary-operation : s-expression -> TFWAE
;; handles the parsing of binary operations
(define (parse-binary-operation sexp)
  (cond
    [(< (length sexp) 3)
     (error "wrong number of operands")]
    [else
     (make-binop (first sexp) (second sexp) (third sexp))]))

;; parse-with-expr : s-expression -> TFWAE
;; handles the parsing of with expressions
(define (parse-with-expr sexp)
  (cond
    [(< (length sexp) 3)
     (error "malformed with expression")]
    [(not (list? (second sexp)))
     (error 'parse-with-expr "expected (id expr), given: ~s" (second sexp))]
    [else
     (let ([bind-symbol (first (second sexp))]
           [bind-expr (second (second sexp))]
           [body (third sexp)])
       (cond
         [(not (symbol? bind-symbol))
          (error 'parse-with-expr "expected an identifier, given: ~s" bind-symbol)]
         [else
          (with bind-symbol (parse bind-expr) (parse body))]))]))

;; parse-if0-expr : s-expression -> TFWAE
;; handles the parsing of if0 expressions
(define (parse-if0-expr sexp)
  (let ([test-expr (second sexp)]
        [then-expr (third sexp)]
        [else-expr (fourth sexp)])
    (if0 (parse test-expr) (parse then-expr) (parse else-expr))))

;; parse-type : s-expression -> Type
;; handles the parsing of types
(define (parse-type t-expr)
  (cond
    [(not (or (symbol? t-expr) (list? t-expr)))
     (error "expected a type expression")]
    [(equal? t-expr 'number)
     (t-num)]
    [(list? t-expr)
     (if (< (length t-expr) 3)
         (error "malformed type expression")
         (t-fun (parse-type (first t-expr)) (parse-type (third t-expr))))]
    [(not (tfwae-basic-type? t-expr))
     (error 'parse-type "unknown type: ~s" t-expr)]
    [else
     (error "bad type expression")]))

;; parse-fun-expr : s-expression -> TFWAE
;; handles the parsing of fun expressions
(define (parse-fun-expr sexp)
  (cond
    [(< (length sexp) 5)
     (error "malformed fun expression")]
    [(not (list? (second sexp)))
     (error "expected a typed formal parameter")]
    [(< (length (second sexp)) 3)
     (error "malformed typed formal parameter")]
    [(not (symbol? (first (second sexp))))
     (error "expected an identifier")]
    [else
     (let ([argid (first (second sexp))]
           [argtype (third (second sexp))]
           [resulttype (fourth sexp)]
           [body (fifth sexp)])
       (fun argid (parse-type argtype) (parse-type resulttype) (parse body)))]))

;; parse-app : s-expression -> TFWAE
;; handles parsing function applications
(define (parse-app sexp)
  (cond
    [(> (length sexp) 2)
     (error "malformed function application")]
    [else
     (let ([fun-expr (first sexp)]
           [fun-args (second sexp)])
       (app (parse fun-expr) (parse fun-args)))]))

; parse : s-expression -> TFWAE
(define (parse sexp)
  (cond
    [(boolean? sexp)
     (error "invalid datatype in input")]
    [(symbol? sexp)
     (id sexp)]
    [(number? sexp)
     (num sexp)]
    [(list? sexp)
     (cond
       [(tfwae-op? (first sexp))
        (parse-binary-operation sexp)]
       [(case (first sexp)
          [(with)
           (parse-with-expr sexp)]
          [(if0)
           (parse-if0-expr sexp)]
          [(fun)
           (parse-fun-expr sexp)]
          [else
           (parse-app sexp)])])]))

;; typeof-env : TFWAE type-environment -> type
;; Attempts to determine the given TFWAE's Type under the given type environment.
;; A type environemnt is an association list, mapping identifiers to types.
;; This is used when we have a function application or a with expression.
;; an env looks like this:
;; (list (list 'x (t-num)))
(define (typeof-env expr env)
  (type-case TFWAE expr
    [id (x)
        (second (assoc x env))]
    [num (n)
         (t-num)]
    [fun (arg-id arg-type result-type body)
         (let ([funargid-lookup (assoc arg-id env)])
           (if (not funargid-lookup)
               (typeof-env expr (append env (list (list arg-id arg-type))))
               (t-fun arg-type (typeof-env body env))))]
    [binop (op l r)
           (if (and
                (type=? (typeof-env l env) (t-num))
                (type=? (typeof-env r env) (t-num)))
               (t-num)
               (error "type mismatch: operands must be numbers"))]
    [with (name named-expr body)
          (let ([name-lookup (assoc name env)])
            (if (not name-lookup)
                {begin
                  ;{display (append env (list (list name (type-of named-expr))))} {newline}
                  (typeof-env expr (append env (list (list name (type-of named-expr)))))}
                {begin
                  {display body} {newline}
                  {display env} {newline}
                  (typeof-env body env)}))]
    [app (fun-expr arg-expr)
         (t-fun-result (typeof-env fun-expr env))]
    [else
     (error "typeof-env not done!")]))

;; type-of : TFWAE -> Type
(define (type-of expr)
  (type-case TFWAE expr
    [num (n)
         (t-num)]
    [id (x)
        (error 'type-of "free identifier: ~s" x)]
    [fun (arg-id arg-type result-type body)
         (let ([actual-type (typeof-env expr empty)])
           (if (type=? actual-type (t-fun arg-type result-type))
               actual-type
               (error "type mismatch: function body has wrong type")))]
    [binop (op l r)
           (if (and (type=? (type-of l) (t-num)) (type=? (type-of r) (t-num)))
               (t-num)
               (error "type mismatch: operands must be numbers"))]
    [app (fun-expr arg-expr)
         (cond
           [(not (or (id? fun-expr) (fun? fun-expr)))
            (error "expected a function")]
           [else
            (let* ([typeof-funexpr-env (typeof-env fun-expr empty)] ;typeof-env gives a t-fun
                   [actual-argtype (type-of arg-expr)]
                   [formal-argtype (t-fun-arg typeof-funexpr-env)]
                   [actual-result-type (t-fun-result typeof-funexpr-env)]
                   [formal-result-type (fun-result-type fun-expr)])
              (if (type=? actual-argtype formal-argtype)
                  actual-result-type
                  (error "type mismatch: actual parameter has wrong type")))])]
    [with (name named-expr body)
          (typeof-env expr empty)]
    [else
     (error "type-of not done!")]))
