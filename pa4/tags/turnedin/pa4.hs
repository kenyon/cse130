-- pa4.hs by Kenyon Ralph <kralph@ucsd.edu>
-- Part 1 ----------------------------------------------------------------------

merge :: (t -> t -> Bool) -> [t] -> [t] -> [t]
merge p x y | null x = y
	    | null y = x
	    | p (head x) (head y) = (head x) : merge p (tail x) y
	    | otherwise = (head y) : merge p x (tail y)

split :: [a] -> ([a], [a])
split x = splitAt (length x `div` 2) x

mergeSort :: (a -> a -> Bool) -> [a] -> [a]
mergeSort p x
    | (length x) <= 1 = x
    | otherwise = merge p (mergeSort p (fst (split x))) (mergeSort p (snd (split x)))

mergeSortDecreasing :: [Integer] -> [Integer]
mergeSortDecreasing = mergeSort (>)

-- A True value in the list means the test passed.
-- Define more tests of your own by adding expressions to the list!
-- Fortunately GHCi's tab-completion saves you from typing the whole name. :)
part1testResults = [
  split [1,2,3,4,5] == ([1,2],[3,4,5]),
  split [1,2,3,4,5,6] == ([1,2,3],[4,5,6]),
  split ([]::[()]) == ([],[]),
  split [99] == ([],[99]),
  merge (<) [1,3] [2,4] == [1,2,3,4],
  merge (>) [3,1] [4,2] == [4,3,2,1],
  merge (<) [1,3,5,99] [2,4,9,20] == [1,2,3,4,5,9,20,99],
  merge (<) [1,3,5,99] [] == [1,3,5,99],
  merge (<) [] [1,3,5,99] == [1,3,5,99],
  --merge (<) [] [] == [],
  mergeSortDecreasing [2,1,3] == [3,2,1]]

-- Part 2 ----------------------------------------------------------------------

divides :: (Integral a) => a -> a -> Bool
divides x y | y `rem` x == 0 = True
	    | otherwise = False

divisors :: (Integral a) => a -> [a]
divisors x = [1] ++ (filter (\ y -> y `divides` x )  [2..(x-1)]) ++ [x]

isPrime :: (Integral a) => a -> Bool
isPrime x | x == 1 = False
	  | x == 2 = True
	  | (x `rem` 2) == 0 = False
	  | length (divisors x) == 2 = True
	  | otherwise = False

primes :: [Integer]
primes = filter isPrime ([2] ++ [3,5..])

part2testResults = [
  divides 2 4,
  divisors 3 == [1,3],
  not (isPrime 1),
  primes !! 499 == 3571,
  take 58 primes == [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,
		     61,67,71,73,79,83,89,97,101,103,107,109,113,127,
		     131,137,139,149,151,157,163,167,173,179,181,191,
		     193,197,199,211,223,227,229,233,239,241,251,257,
		     263,269,271]]
--first 58 primes from http://www.research.att.com/~njas/sequences/table?a=40&fmt=4
--500th prime from http://en.wikipedia.org/wiki/List_of_prime_numbers#The_first_500_prime_numbers

-- Part 3 ----------------------------------------------------------------------

data BinaryTree a b = Branch a (BinaryTree a b) (BinaryTree a b)
                    | Leaf b deriving (Show, Eq)
data BinOp = Add | Sub | Mult
instance Show BinOp where
  show Add = "+"
  show Sub = "-"
  show Mult = "*"

getFunction :: (Num a) => BinOp -> a -> a -> a
getFunction Add x y = x + y
getFunction Sub x y = x - y
getFunction Mult x y = x * y

showParenPre :: (Show a) => a -> [Char] -> [Char] -> [Char]
showParenPre x str1 str2 = "(" ++ (show x) ++ " " ++ str1 ++ " " ++ str2 ++ ")"

showPost :: (Show a) => a -> [Char] -> [Char] -> [Char]
showPost x str1 str2 = str1 ++ " " ++ str2 ++ " " ++ (show x)

treeApply :: (t -> t2 -> t2 -> t2) -> (t1 -> t2) -> BinaryTree t t1 -> t2
treeApply s1 s2 (Branch x y z) = s1 x (treeApply s1 s2 y) (treeApply s1 s2 z)
treeApply s1 s2 (Leaf x) = s2 x

-- These functions use the ones you define above.
showScheme :: (Show a, Show b) => BinaryTree a b -> String
showScheme = treeApply showParenPre show

showRPN :: (Show a, Show b) => BinaryTree a b -> String
showRPN = treeApply showPost show

eval :: BinaryTree BinOp Integer -> Integer
eval = treeApply getFunction id

part3testResults = [
  eval exp1 == 5,
  showScheme exp1 == "(- (* 3 2) 1)",
  showRPN exp1 == "3 2 * 1 -",
  showRPN exp2 == "3 2 *",
  showRPN exp3 == "2"]
    where
      exp1 = Branch Sub (Branch Mult (Leaf 3) (Leaf 2)) (Leaf 1)
      exp2 = Branch Mult (Leaf 3) (Leaf 2)
      exp3 = (Leaf 2) :: (Num t) => BinaryTree BinOp t
