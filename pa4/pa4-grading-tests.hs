{-
To simulate how we graded PA4, refer to the tests below and do the following:

   1. Paste the test code into a file called PA4Tests.hs
   2. Copy your solutions into a file called PA4.hs
   3. Add a line to the top of PA4.hs as follows: module PA4 where
   4. Run the test code using runhaskell PA4Tests.hs 

Here is the test code (thanks to Steve):
-}

module PA4Tests where
import Test.HUnit
import PA4

tests =
  -------------
  --  Part 1 --
  -------------
  -- split
  [ "split" ~: "split []"    ~: ([]::[Int],[]::[Int]) ~=? split []
  , "split" ~: "split [1]"   ~: ([],[1])              ~=? split [1]
  , "split" ~: "split [1,2]" ~: ([1],[2])             ~=? split [1,2]
  -- merge
  , "merge" ~: "merge (<) [] []"          ~: ([]::[Int]) ~=? merge (<) [] []
  , "merge" ~: "merge (<) [] [1]"         ~: [1]         ~=? merge (<) [] [1]
  , "merge" ~: "merge (<) [1] []"         ~: [1]         ~=? merge (<) [1] []
  , "merge" ~: "merge (<) [1,2] [3,4]"    ~: [1..4]      ~=? merge (<) [1,2] [3,4]
  , "merge" ~: "merge (<) [1,3] [2,4]"    ~: [1..4]      ~=? merge (<) [1,3] [2,4]
  , "merge" ~: "merge (>) [] []"          ~: ([]::[Int]) ~=? merge (>) [] []
  , "merge" ~: "merge (>) [] [1]"         ~: [1]         ~=? merge (>) [] [1]
  , "merge" ~: "merge (>) [1] []"         ~: [1]         ~=? merge (>) [1] []
  , "merge" ~: "merge (>) [2,1] [4,3]"    ~: [4,3..1]    ~=? merge (>) [2,1] [4,3]
  , "merge" ~: "merge (>) [3,1] [4,2]"    ~: [4,3..1]    ~=? merge (>) [3,1] [4,2]
  , "merge" ~: "merge (<) \"hi\" \"bey\"" ~: "behiy"     ~=? merge (<) "hi" "bey"
  -- mergeSort
  , "mergeSort" ~: "mergeSort (<) []"         ~: ([]::[Int]) ~=? mergeSort (<) []
  , "mergeSort" ~: "mergeSort (<) [1]"        ~: [1]         ~=? mergeSort (<) [1]
  , "mergeSort" ~: "mergeSort (<) [1,2]"      ~: [1,2]       ~=? mergeSort (<) [1,2]
  , "mergeSort" ~: "mergeSort (<) [2,1]"      ~: [1,2]       ~=? mergeSort (<) [2,1]
  , "mergeSort" ~: "mergeSort (<) [10,9..1]"  ~: [1..10]     ~=? mergeSort (<) [10,9..1]
  , "mergeSort" ~: "mergeSort (<) \"hello\""  ~: "ehllo"     ~=? mergeSort (<) "hello"
  , "mergeSort" ~: "mergeSort (>) []"         ~: ([]::[Int]) ~=? mergeSort (>) []
  , "mergeSort" ~: "mergeSort (>) [1]"        ~: [1]         ~=? mergeSort (>) [1]
  , "mergeSort" ~: "mergeSort (>) [1,2]"      ~: [2,1]       ~=? mergeSort (>) [1,2]
  , "mergeSort" ~: "mergeSort (>) [2,1]"      ~: [2,1]       ~=? mergeSort (>) [2,1]
  , "mergeSort" ~: "mergeSort (>) [1..10]"    ~: [10,9..1]   ~=? mergeSort (>) [1..10]
  , "mergeSort" ~: "mergeSort (>) \"hello\""  ~: "ollhe"     ~=? mergeSort (>) "hello"
  -------------
  --  Part 2 --
  -------------
  -- divides
  , "divides" ~: "divides  1 12" ~: True  ~=? divides  1 12
  , "divides" ~: "divides  2 12" ~: True  ~=? divides  2 12
  , "divides" ~: "divides  3 12" ~: True  ~=? divides  3 12
  , "divides" ~: "divides  4 12" ~: True  ~=? divides  4 12
  , "divides" ~: "divides  5 12" ~: False ~=? divides  5 12
  , "divides" ~: "divides  6 12" ~: True  ~=? divides  6 12
  , "divides" ~: "divides  7 12" ~: False ~=? divides  7 12
  , "divides" ~: "divides  8 12" ~: False ~=? divides  8 12
  , "divides" ~: "divides  9 12" ~: False ~=? divides  9 12
  , "divides" ~: "divides 10 12" ~: False ~=? divides 10 12
  , "divides" ~: "divides 11 12" ~: False ~=? divides 11 12
  , "divides" ~: "divides 12 12" ~: True  ~=? divides 12 12
  , "divides" ~: "divides  0  0" ~: True  ~=? divides  0  0
  , "divides" ~: "divides  1  0" ~: True  ~=? divides  1  0
  , "divides" ~: "divides  2  0" ~: True  ~=? divides  2  0
  , "divides" ~: "divides  0  1" ~: False ~=? divides  0  1
  , "divides" ~: "divides  0  2" ~: False ~=? divides  0  2
  -- divisors
  , "divisors" ~: "divisors 12" ~: [1,2,3,4,6,12] ~=? divisors 12
  , "divisors" ~: "divisors  1" ~: [1]            ~=? divisors  1
  , "divisors" ~: "divisors  5" ~: [1,5]          ~=? divisors  5
  -- isPrime
  -- , "isPrime" ~: "isPrime -5" ~: False ~=? isPrime (negate 5)
  , "isPrime" ~: "isPrime  0" ~: False ~=? isPrime 0
  , "isPrime" ~: "isPrime  1" ~: False ~=? isPrime 1
  , "isPrime" ~: "isPrime  2" ~: True  ~=? isPrime 2
  , "isPrime" ~: "isPrime  3" ~: True  ~=? isPrime 3
  , "isPrime" ~: "isPrime  4" ~: False ~=? isPrime 4
  -- primes
  , "primes" ~: "take 10 primes" ~: [2,3,5,7,11,13,17,19,23,29] ~=? take 10 primes
  , "primes" ~: "primes !! 100"  ~: 547                         ~=? primes !! 100
  -------------
  --  Part 3 --
  -------------
  -- getFunction
  , "getFunction" ~: "getFunction Add  5 4" ~:  9 ~=? getFunction Add  5 4
  , "getFunction" ~: "getFunction Sub  5 4" ~:  1 ~=? getFunction Sub  5 4
  , "getFunction" ~: "getFunction Mult 5 4" ~: 20 ~=? getFunction Mult 5 4
  -- showParenPre
  , "showParenPre" ~: "showParenPre 5 \"foo\" \"bar\""       ~: "(5 foo bar)"       ~=? showParenPre 5 "foo" "bar"
  , "showParenPre" ~: "showParenPre \"baz\" \"foo\" \"bar\"" ~: "(\"baz\" foo bar)" ~=? showParenPre "baz" "foo" "bar"
  -- showPost
  , "showPost" ~: "showPost 5 \"foo\" \"bar\""       ~: "foo bar 5"       ~=? showPost 5 "foo" "bar"
  , "showPost" ~: "showPost \"baz\" \"foo\" \"bar\"" ~: "foo bar \"baz\"" ~=? showPost "baz" "foo" "bar"
  -- treeApply
  , "treeApply" ~: "eval (Leaf 5)"            ~:  5 ~=? eval (Leaf 5)
  , "treeApply" ~: "eval (" ++ show t1 ++ ")" ~: 12 ~=? eval t1
  , "treeApply" ~: "eval (" ++ show t2 ++ ")" ~:  6 ~=? eval t2
  , "treeApply" ~: "eval (" ++ show t3 ++ ")" ~: 72 ~=? eval t3
  ] where
    t1 = Branch Add (Leaf 5) (Leaf 7)
    t2 = Branch Sub (Branch Mult (Leaf 6) (Leaf 3)) t1
    t3 = Branch Mult t1 t2
    t4 = Branch Sub t3 t2
    t5 = Branch Sub t2 t1
    t6 = Branch Add t4 t5


main = do
  Counts c t e f <- runTestTT $ TestList tests
  putStr . show $ t - e - f
  putStr " of "
  putStr . show $ c
  putStr " passed, score: "
  putStr . show . round $ 35 + (fromIntegral (t-e-f))*30/(fromIntegral c)
  putStrLn ""
