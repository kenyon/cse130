#lang scheme
(require "pa1.ss" (planet "test.ss" ("schematics" "schemeunit.plt" 2 10)))
(require (planet "text-ui.ss" ("schematics" "schemeunit.plt" 2 10)))
; When first you run this, it will fetch and install SchemeUnit automatically.
; SchemeUnit documentation can be found at the following URL:
; http://planet.plt-scheme.org/package-source/schematics/schemeunit.plt/2/10/doc.txt

; pa1-tests.ss by Kenyon Ralph <kralph@ucsd.edu>

(define pa1-tests
  (test-suite
   "pa1"
   (test-equal? "list-head" (list-head '(a b c) 2) '(a b))
   (test-equal? "list-head" (list-head '(b c) 1) '(b))
   (test-equal? "list-head" (list-head '(a b c) 0) '())
   (test-equal? "list-head" (list-head '() 0) '())
   (test-equal? "list-head" (list-head '(a b c d e f g) 7) '(a b c d e f g))
   (test-equal? "split-position" (split-position '(a b c)) 1)
   (test-equal? "split-position" (split-position '(a b c d)) 2)
   (test-equal? "split-position" (split-position '(a b c d e f)) 3)
   (test-equal? "split-position" (split-position '()) 0)
   (test-equal? "first-half" (first-half '()) '())
   (test-equal? "first-half" (first-half '(a)) '()) ;empty is the shorter half of a 1-element list
   (test-equal? "first-half" (first-half '(a b c)) '(a))
   (test-equal? "first-half" (first-half '(a b c d e f g)) '(a b c))
   (test-equal? "first-half" (first-half '(a b c d e f)) '(a b c))
   (test-equal? "second-half" (second-half '()) '())
   (test-equal? "second-half" (second-half '(a)) '(a))
   (test-equal? "second-half" (second-half '(a b c)) '(b c))
   (test-equal? "second-half" (second-half '(a b c d e f g)) '(d e f g))
   (test-equal? "second-half" (second-half '(a b c d e f)) '(d e f))
   (test-equal? "merge" (merge < '(1 3) '(2 4)) '(1 2 3 4))
   (test-equal? "merge" (merge < '(0) '(0)) '(0 0))
   (test-equal? "merge" (merge < '(2) '(1)) '(1 2))
   (test-equal? "merge" (merge < '() '()) '())
   (test-equal? "merge" (merge < '(9) '()) '(9))
   (test-equal? "merge" (merge < '() '(8)) '(8))
   (test-equal? "merge" (merge < '(3 5 7) '(2 4 10 12)) '(2 3 4 5 7 10 12))
   (test-equal? "merge-sort" (merge-sort < '(4 3 2 1)) '(1 2 3 4))
   (test-equal? "merge-sort" (merge-sort < '(9 4 8 3 12 2 7 1)) '(1 2 3 4 7 8 9 12))
   (test-equal? "merge-sort" (merge-sort < '(7)) '(7))
   (test-equal? "merge-sort" (merge-sort < '()) '())
   (test-equal? "sort-numbers-decreasing"
                (sort-numbers-decreasing  '(1 2 3 4)) '(4 3 2 1))
   (test-equal? "sort-numbers-decreasing"
                (sort-numbers-decreasing  '()) '())
   (test-equal? "sort-numbers-decreasing"
                (sort-numbers-decreasing  '(9 9 9 9 9)) '(9 9 9 9 9))
   (test-equal? "sort-numbers-decreasing"
                (sort-numbers-decreasing '(50 10 6 20 30 0 40 100 25))
		'(100 50 40 30 25 20 10 6 0))
   (test-equal? "sort-strings-increasing"
                (apply string-append
                 (sort-strings-increasing  '("job " "mate!" "Good ")))
                "Good job mate!")
   (test-equal? "sort-strings-increasing"
                (apply string-append
                 (sort-strings-increasing '("z" "a " "b " "r " "l ")))
                "a b l r z")
   (test-equal? "sort-strings-increasing" (sort-strings-increasing '("z" "a" "b" "r" "l"))
                '("a" "b" "l" "r" "z"))
   (test-equal? "sort-strings-increasing" (sort-strings-increasing '("LOL" ""))
                '("" "LOL"))
   (test-equal? "sort-strings-increasing" (sort-strings-increasing '("LOL" "lol" "lolz" "LOLLERSKATES" "LOLLER"))
                '("LOL" "LOLLER" "LOLLERSKATES" "lol" "lolz"))
   (test-equal? "sort-strings-increasing" (sort-strings-increasing '()) '())))

(test/text-ui pa1-tests)
