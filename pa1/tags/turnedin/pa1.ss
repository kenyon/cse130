#lang scheme
(provide (all-defined-out))
;; Don't change the lines above unless you know what you're doing!
;; The functions below can be short; none of mine is longer than 8 lines.
;; -- Dana

; pa1.ss by Kenyon Ralph <kralph@ucsd.edu>

;; list-head : list number -> list
;; Returns a list of the first pos elements of lst, in the same order.
;; You may assume the number is a non-negative integer no greater than
;; the length of the list. For bonus points, throw appropriate exceptions!
(define (list-head lst pos)
    (if (zero? pos)
      empty
      (cons (first lst) (list-head (rest lst) (sub1 pos)))))

  ;other ways to do list-head:
  ;(reverse (list-tail (reverse lst) (- (length lst) pos))))
  ;(take lst pos)) ;built-in way. Actually called list-head in MIT/GNU Scheme.

;; split-position : list -> number
;; Returns the position immediately before the midpoint in the list.
(define (split-position lst)
  (quotient (length lst) 2))

;; first-half : list -> list
;; Returns the first half of the list, as nearly as possible.
;; When the list is odd in length, the first half is the shorter.
(define (first-half lst)
  (list-head lst (split-position lst)))

;; second-half : list -> list
;; Returns the second half of the list, as nearly as possible.
;; When the list is odd in length, the second half is the longer.
(define (second-half lst)
  (list-tail lst (split-position lst)))

;; merge : predicate list list -> list
;; Given a predicate p and 2 lists sorted according to p,
;; merges the lists into a single list sorted according to p.
(define (merge p left right)
  (cond
    [(empty? left) (append right)]
    [(empty? right) (append left)]
    [else
      (if (p (first left) (first right))
	(cons (first left) (merge p (rest left) right))
	(cons (first right) (merge p (rest right) left)))]))

;; merge-sort : predicate list -> list
;; Given a predicate p and a list lst,
;; returns a list of the elements of lst sorted according to p.
(define (merge-sort p lst)
  (if (<= (length lst) 1)
    lst
    (merge p (merge-sort p (first-half lst)) (merge-sort p (second-half lst)))))

;; sort-numbers-decreasing : list-of-nums -> list-of-nums
;; Given a list of numbers, returns a list of those numbers in decreasing order.
;; (OK, non-increasing order technically, but the name is long enough already!)
;; (Hint: This is very simple using curry and merge-sort!)
(define sort-numbers-decreasing
  (curry merge-sort >))

;; sort-strings-increasing : list-of-strings -> list-of-strings
;; Given a list of strings, returns a list of those strings in increasing order.
(define sort-strings-increasing
  (curry merge-sort string<?))
