20080702 CSE 130 lecture notes

(define lst '(1 2 3))
(apply + lst)



#lang scheme

(define lst '(1 2 3))

(define (apply-fu proc-name lst)
  (eval (cons proc-name lst)))

(define (add1 x) (+ 1 x))

(define (map-fu proc lst)
  (if (empty? lst) empty
      (cons (proc (first lst)) (map-fu proc (rest lst)))))



=== currying ===

> (curry *)
#<procedure:curried>
> ((curry *) 2)
#<procedure:curried>
> (((curry *) 2) 3)
6
> 

> (define add1toeach ((curry map) add1))
> (add1toeach '(1 2 3 4))
(2 3 4 5)
> 


> ((curry * 2) 3)
6


filter. http://docs.plt-scheme.org/guide/Lists__Iteration__and_Recursion.html
negate. http://docs.plt-scheme.org/reference/procedures.html#(def._((lib._scheme/function..ss)._negate))




(define mixed-list '(a 1 b 2 "str"))
(define just-numbers
  ((curry filter) number?))

> mixed-list
(a 1 b 2 "str")
> (just-numberes mixed-list)
. . reference to an identifier before its definition: just-numberes
> (just-numbers mixed-list)
(1 2)


#lang scheme

(define (mycurry proc)
  (lambda (a)
    (lambda b
      (apply proc (cons a b)))))

(define mixed-list '(a 1 b 2 "str"))


(define just-numbers
  ((mycurry filter) number?))

> (just-numbers mixed-list)
(1 2)
> 


(define (uncurry proc)
  (lambda (f . r)
    (apply (proc f) r)))


strongly typed vs weakly typed.
strong: types checked before runtime.
weak: types checked at runtime.

read about foldr, foldl.
