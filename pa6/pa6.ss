#lang plai

(define-type TRCFWAE
  [num (n number?)]
  [binop (op procedure?) (lhs TRCFWAE?) (rhs TRCFWAE?)]
  [with (name symbol?) (named-expr TRCFWAE?) (body TRCFWAE?)]
  [id (name symbol?)]
  [fun (arg-id symbol?) (arg-type Type?) (result-type Type?) (body TRCFWAE?)]
  [app (fun-expr TRCFWAE?) (arg-expr TRCFWAE?)]
  [bool (b symbol?)]
  [if-true (c TRCFWAE?) (t TRCFWAE?) (e TRCFWAE?)]
  [rec (name symbol?) (fun-type t-fun?) (named-expr TRCFWAE?) (body TRCFWAE?)])

(define-type Type
  [t-num]
  [t-bool]
  [t-fun (arg Type?) (result Type?)])

;; the basic types used in this language
(define trcfwae-basic-types
  '(number boolean))

;; trcfwae-basic-type? : symbol -> boolean
;; true only if the symbol is a basic type of this language
(define (trcfwae-basic-type? sym)
  (if (member sym trcfwae-basic-types)
      #t
      #f))

;; type=? : Type Type -> boolean
(define (type=? t1 t2)
  (equal? t1 t2))

;; the keywords used in this language
(define trcfwae-keywords
  '(fun with if rec true false))

;; trcfwae-keyword? : symbol -> boolean
;; true only if the symbol is a syntactic keyword of this language
(define (trcfwae-keyword? sym)
  (if (member sym trcfwae-keywords)
      #t
      #f))

;; the operators used in this language
(define trcfwae-operators
  (list
   (list '+ +)
   (list '- -)
   (list '* *)
   (list '/ /)))

;; trcfwae-op? : symbol -> boolean
;; true only if the symbol is an operator name
(define (trcfwae-op? sym)
  (if (assoc sym trcfwae-operators)
      #t
      #f))

;; trcfwae-reserved? : symbol -> boolean
;; true only if the symbol is a keyword or operator name
(define (trcfwae-reserved? sym)
  (or (trcfwae-op? sym) (trcfwae-keyword? sym)))

;; get-binary-operator : symbol -> procedure
;; example: (get-binary-operator '*) -> #<procedure:*>
(define (get-binary-operator opsymbol)
  (second (assoc opsymbol trcfwae-operators)))

;; make-binop : opsymbol, lhs, rhs -> parsed binop
;; example: (make-binop '+ 4 8) -> (binop #<procedure:+> (num 4) (num 8))
;; used in parse
(define (make-binop opsymbol lhs rhs)
  (binop (get-binary-operator opsymbol) (parse lhs) (parse rhs)))

;; parse-binary-operation : s-expression -> TRCFWAE
;; handles the parsing of binary operations
(define (parse-binary-operation sexp)
  (cond
    [(not (equal? (length sexp) 3))
     (error "wrong number of operands")]
    [else
     (make-binop (first sexp) (second sexp) (third sexp))]))

;; parse-with-expr : s-expression -> TRCFWAE
;; handles the parsing of with expressions
(define (parse-with-expr sexp)
  (cond
    [(not (equal? (length sexp) 3))
     (error "malformed with expression")]
    [(not (list? (second sexp)))
     (error 'parse-with-expr "expected (id expr), given: ~s" (second sexp))]
    [(empty? (second sexp))
     (error "binding list cannot be empty")]
    [else
     (let ([bind-symbol (first (second sexp))]
           [bind-expr (second (second sexp))]
           [body (third sexp)])
       (cond
         [(not (symbol? bind-symbol))
          (error 'parse-with-expr "expected an identifier, given: ~s" bind-symbol)]
         [else
          (with bind-symbol (parse bind-expr) (parse body))]))]))

;; parse-type : s-expression -> Type
;; handles the parsing of types
(define (parse-type t-expr)
  (cond
    [(not (or (symbol? t-expr) (list? t-expr)))
     (error "expected a type expression")]
    [(equal? t-expr 'number)
     (t-num)]
    [(equal? t-expr 'boolean)
     (t-bool)]
    [(list? t-expr)
     (if (not (equal? (length t-expr) 3))
         (error "malformed type expression")
         (t-fun (parse-type (first t-expr)) (parse-type (third t-expr))))]
    [(not (trcfwae-basic-type? t-expr))
     (error 'parse-type "unknown type: ~s" t-expr)]
    [else
     (error 'parse-type "bad type expression: ~s" t-expr)]))

;; parse-fun-expr : s-expression -> TRCFWAE
;; handles the parsing of fun expressions
(define (parse-fun-expr sexp)
  (cond
    [(not (equal? (length sexp) 5))
     (error "malformed fun expression")]
    [(not (list? (second sexp)))
     (error "expected a typed formal parameter")]
    [(not (equal? (length (second sexp)) 3))
     (error "malformed typed formal parameter")]
    [(not (symbol? (first (second sexp))))
     (error "expected an identifier")]
    [else
     (let ([argid (first (second sexp))]
           [argtype (third (second sexp))]
           [resulttype (fourth sexp)]
           [body (fifth sexp)])
       (fun argid (parse-type argtype) (parse-type resulttype) (parse body)))]))

;; parse-app : s-expression -> TRCFWAE
;; handles parsing function applications
(define (parse-app sexp)
  (cond
    [(not (equal? (length sexp) 2))
     (error "malformed function application")]
    [else
     (let ([fun-expr (first sexp)]
           [fun-args (second sexp)])
       (app (parse fun-expr) (parse fun-args)))]))

;; parse-if-expr : s-expression -> TRCFWAE
;; handles parsing if expressions
(define (parse-if-expr sexp)
  (cond
    [(not (equal? (length sexp) 4))
     (error "malformed if expression")]
    [else
     (let ([test-expr (second sexp)]
           [then-expr (third sexp)]
           [else-expr (fourth sexp)])
       (if-true (parse test-expr) (parse then-expr) (parse else-expr)))]))

;; parse-rec-expr : s-expression -> TRCFWAE
;; handles parsing rec expressions
(define (parse-rec-expr sexp)
  (cond
    [(not (equal? (length sexp) 3))
     (error "malformed rec expression")]
    [(not (list? (second sexp)))
     (error "expected a typed cyclic binding")]
    [(not (equal? (length (second sexp)) 4))
     (error "malformed typed cyclic binding")]
    [(not (symbol? (first (second sexp))))
     (error 'parse-rec-expr "expected an identifier, given: ~s" (first (second sexp)))]
    [(not (equal? ': (second (second sexp))))
     (error 'parse-rec-expr "expected a colon, given: ~s" (second (second sexp)))]
    [else
     (let ([name (first (second sexp))]
           [funtype (parse-type (third (second sexp)))]
           [namedexpr (parse (fourth (second sexp)))]
           [body (parse (third sexp))])
       (if (t-fun? funtype)
           (rec name funtype namedexpr body)
           (error "cyclic binding must have a function type")))]))

; parse : s-expression -> TRCFWAE
(define (parse sexp)
  (cond
    [(or (boolean? sexp) (string? sexp) (char? sexp))
     (error "invalid datatype in input")]
    [(symbol? sexp)
     (id sexp)]
    [(number? sexp)
     (num sexp)]
    [(list? sexp)
     (cond
       [(empty? sexp)
        (error "unexpected empty list")]
       [(trcfwae-op? (first sexp))
        (parse-binary-operation sexp)]
       [(case (first sexp)
          [(with)
           (parse-with-expr sexp)]
          [(fun)
           (parse-fun-expr sexp)]
          [(if)
           (parse-if-expr sexp)]
          [(rec)
           (parse-rec-expr sexp)]
          [else
           (parse-app sexp)])])]))

;; typeof-env : TRCFWAE type-environment -> type
;; Attempts to determine the given TRCFWAE's Type under the given type environment.
;; A type environemnt is an association list, mapping identifiers to types.
;; This is used when we have a function application or a with expression.
;; an env looks like this:
;; (list (list 'x (t-num)))
(define (typeof-env expr env)
  (type-case TRCFWAE expr
    [num (n)
         (t-num)]
    [binop (op l r)
           (if (and
                (type=? (typeof-env l env) (t-num))
                (type=? (typeof-env r env) (t-num)))
               (t-num)
               (error "type mismatch: operands must be numbers"))]
    [with (name named-expr body)
          (typeof-env body (append env (list (list name (typeof-env named-expr env)))))]
    [id (x)
        (let ([lookup (assoc x env)])
          (if lookup
              (second lookup)
              (error "free identifier")))]
    [fun (arg-id arg-type result-type body)
         (let ([funargid-lookup (assoc arg-id env)])
           (if funargid-lookup
               (t-fun arg-type (typeof-env body env))
               (typeof-env expr (append env (list (list arg-id arg-type))))))]
    [app (fun-expr arg-expr)
         (cond
           [(and
             (equal? fun-expr (id 'is-zero))
             (not (type=? (t-num) (typeof-env arg-expr env))))
            (error "type mismatch: actual parameter has wrong type")]
           [else
            (t-fun-result (typeof-env fun-expr env))])]
    [if-true (test-expr then-expr else-expr)
             (let ([typeof-test-expr (typeof-env test-expr env)]
                   [typeof-then-expr (typeof-env then-expr env)]
                   [typeof-else-expr (typeof-env else-expr env)])
               (cond
                 [(not (type=? typeof-test-expr (t-bool)))
                  (error "type mismatch: test expression must have type boolean")]
                 [else
                  (if (type=? typeof-then-expr typeof-else-expr)
                      typeof-then-expr
                      (error "type mismatch: then and else expressions must have same type"))]))]
    [rec (name nametype namedexpr body) ;name=i, nametype=taui, namedexpr=v
      (let* ([tau (typeof-env body (append env (list (list name nametype))))] ;assuming nametype is legal
             [taui (typeof-env namedexpr (append env (list (list name nametype))))]) ;used to guarantee that nametype is legal
        (if (type=? taui nametype) ;check the guarantee
            tau
            (error "type mismatch: named expression must have declared type")))]
    [else
     (error "typeof-env not done!")]))

;; type-of : TRCFWAE -> Type
(define (type-of expr)
  (typeof-env
   expr
   (list ;; Here is the associative list that is the initial type environment.
    (list 'true (t-bool))
    (list 'false (t-bool))
    (list 'is-zero (t-fun (t-num) (t-bool))))))
