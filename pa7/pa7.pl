%% Programming Assignment 7: Type Inference in Prolog

%% pa7.pl by Kenyon Ralph <kralph@ucsd.edu>

%% BNF grammar:
%
%<expr> ::= num(<number>)
%         | add(<expr>,<expr>)
%         | with(<name>,<expr>,<expr>)
%         | id(<name>)
%         | fun(<name>,<expr>)
%         | app(<expr>,<expr>)
%         | bool(t) | bool(f)
%         | if(<expr>,<expr>,<expr>)
%         | rec(<name>,<expr>,<expr>)
%
%<type> ::= t_num
%         | t_bool
%         | t_fun(<type>,<type>)
%
%Above, <number> is any Prolog number, and <name> is any Prolog constant.

%% type(type_environment, expression, type_of_expression)

type(_,num(N), t_num) :- number(N).

type(_, bool(B), t_bool) :- B == t ; B == f.

type(TEnv, add(Left, Right), t_num) :-
    type(TEnv, Left, t_num),
    type(TEnv, Right, t_num).

type(TEnv, if(Test, Then, Else), Tau) :-
    type(TEnv, Test, t_bool),
    type(TEnv, Then, Tau),
    type(TEnv, Else, Tau).

type([bind(I, T)|_], id(I), T) :- !.
type([bind(_, _)|TEnvRest], id(I), T):-
    type(TEnvRest, id(I), T).

% below are untested so far. (20080729)
type([let_bind(X, T)|_], id(X), T1) :- copy_term(T, T1).
type([let_bind(I, T)|_], id(I), T) :- !.
type([let_bind(_, _)|TEnvRest], id(I), T):-
    type(TEnvRest, id(I), T).

type(TEnv, with(Name, Namedexpr, Body), Tau) :-
    type(TEnv, Namedexpr, Tau_ne),
    type([bind(Name, Tau_ne)|TEnv], Body, Tau).

type(TEnv, rec(Name, Namedexpr, Body), Tau) :-
    type([bind(Name, Tau_ne)|TEnv], Namedexpr, Tau_ne),
    type([bind(Name, Tau_ne)|TEnv], Body, Tau).

type(TEnv, fun(Id, Body), t_fun(T1, T2)) :-
    type([bind(Id, T1)|TEnv], Body, T2).

type(TEnv, app(Fun, Arg), T2) :-
    type(TEnv, Fun, t_fun(T1, T2)),
    type(TEnv, Arg, T1).

type_of(Expr, T) :-
    type([bind(is_zero, t_fun(t_num, t_bool))], Expr, T).

%% Some tests here are adapted from PLAI.
%% Adapt the rest of the tests from PLAI, and write your own.
:- begin_tests(basic).
test(num_literals) :-
    setof(T, type_of(num(0),T), [A]), A==t_num,
    not(type_of(num(foo),_)).

test(bool_literals) :-
    setof(T, type_of(bool(t),T), [A]), A==t_bool,
    not(type_of(bool(foo),_)).

test(fun_const) :-
    setof(T, type_of(fun(x,num(0)),T), [t_fun(A,B)]), var(A), B==t_num.

test(plai_p304_quicktest) :-
    setof(T, type([bind(w,t_bool),bind(v,t_num)],id(v),T), [A]), A==t_num.

test(another_quicktest) :-
    setof(T, type([bind(w, t_bool), bind(v, t_num), bind(x, t_bool), bind(y, t_num)], id(x), T), [A]), A==t_bool.

test(plai_p304_test2) :-
    setof(T, type([], fun(x, if(id(x), num(5), num(9))), T), [A]),
    A==t_fun(t_bool, t_num).

test(plai_p305_test1) :-
    setof(T,
    type([],
        app(fun(x,
            if(app(id(x),bool(t)),
                app(id(x),bool(t)),
                app(id(x),bool(t)))),
         fun(y,id(y))),
     T), [A]),
     A==t_bool.

test(plai_p305_test2) :-
    not(type([],
         app(fun(x,
                 if(app(id(x),bool(t)),
                    app(id(x),12),
                    app(id(x),99))),
             fun(y,x(y))),
         _)).

test(plai_p305_test3) :-
    not(type([], fun(x, app(id(x), id(x))), t_num)).

test(type_of_is_zero) :-
    setof(T, type_of(id(is_zero), T), [A]), A==t_fun(t_num, t_bool).

test(addition) :-
    setof(T, type_of(add(num(12), num(33)), T), [A]), A==t_num.

test(bad_addition) :-
    not(type_of(add(bool(t), num(99)), _)).

test(with_double) :-
    setof(T, type_of(with(xxyzx, num(27), add(id(xxyzx), id(xxyzx))), T), [A]), A==t_num.

test(with_if) :-
    setof(T,
            type_of(with(zarf,
                        if(app(id(is_zero), num(0)), add(num(1), num(1)), add(num(2), num(2))),
                        id(zarf)),
                    T),
                [A]),
            A==t_num.

test(with_bool) :-
    setof(T,
            type_of(with(zorg, num(0), app(id(is_zero), id(zorg))), T),
            [A]),
        A==t_bool.

test(rec_simple) :-
    setof(T, type_of(rec(simple, fun(n, id(n)), id(simple)), T), [t_fun(X, X)]).

test(rec_summation) :-
    setof(T, type_of(rec(summation, fun(n, if(app(id(is_zero), id(n)), num(1), add(id(n), app(id(summation), add(id(n), num(-1)))))), app(id(summation), num(5))), T), [A]), A==t_num.
:- end_tests(basic).

%% This test exposes a bug in the PLAI code, which you should fix.
:- begin_tests(plai_bug).
test(env_shadowing) :-
    setof(T, type([bind(v,t_num),bind(v,t_bool)],id(v),T), [A]), A==t_num.
:- end_tests(plai_bug).

%% The following tests won't even terminate until the previous test passes.
:- begin_tests(more_interesting).
test(fun_if) :-
    setof(T, type_of(fun(x,if(id(x),id(x),id(x))),T),
            [t_fun(A,B)]), A==t_bool, B==t_bool.

test(fun_identity) :-
    setof(T, type_of(fun(x,id(x)),T), [t_fun(A,B)]), var(A), var(B), A==B.

test(with_shadowed) :-
    setof(T, type_of(with(x,bool(t),with(x,num(0),id(x))),T), [A]), A==t_num.

test(rec_curried_mult) :-
    setof(T,
        type_of(rec(mult, fun(x, fun(y,
                            if(app(id(is_zero),id(y)), num(0),
                               add(id(x),
                                   app(app(id(mult),id(x)),
                                       add(id(y),num(-1))))))),
                    id(mult)),
                T),
        [t_fun(A,t_fun(B,C))]), A==t_num, B==t_num, C==t_num.

test(polymorphic_identity) :-
  setof(T,
        type_of(with(g,fun(x,id(x)),
                  if(app(id(g),bool(t)),app(id(g),num(0)),num(0))),
                T),
        [A]), A==t_num.
/*
* Hint: You need to treat with bindings differently from fun bindings. The
* chapter of PLAI titled "Implicit Polymorphism" and this discussion of
* Prolog's copy_term predicate (the okpair example) should help you understand
* why and how.
*
* For yet more points, support let-polymorphism in rec expressions and
* demonstrate this with your own tests.
*/
:- end_tests(more_interesting).
% vim: set et:
