%% ch33examples.pl by Kenyon Ralph <kralph@ucsd.edu>

advisor(barwise,feferman).
advisor(feferman,tarski).
advisor(tarski,lesniewski).
advisor(lesniewski,twardowski).
advisor(twardowski,brentano).
advisor(brentano,clemens).
advisor(brentano,trendelenburg).

advisor(X,Y) :- advisee(Y,X).

advisee(tarski,montague).
advisee(tarski,mostowski).
advisee(tarski,robinson).

ancestor(X,Y) :- advisor(X,Y) ; advisor(X,Z), ancestor(Z,Y).

descendant(X,Y) :- ancestor(Y,X).

sibling(X,Y):-
  advisor(X,Z),
  advisor(Y,Z),
  X \== Y.
