20080703 CSE 130 lecture notes

(define (sum2 a b) (+ a b))


(define (sum . lst)
  (if (empty? lst)
    0
    (sum2 (first lst) (apply sum (rest lst)))))


(define (sum-tr . lst)
  (let helper ((partial-sum 0) (h-lst lst))
    (if (empty? h-lst)
      partial-sum
      (helper (sum2 (first h-lst) partial-sum) (rest h-lst)))))



> (sum-tr 1 2 3)
6


